Giraffe

To run the app first install the npm packages

`npm install`

then start the server using

`npm start`

useful run commands
* `npm install` - installs dependencies
* `npm start` - runs the compiler and a server at the same time, both in "watch mode".
* `npm run build` - runs the TypeScript compiler once.
* `npm run build:w` - runs the TypeScript compiler in watch mode; the process keeps running, awaiting changes to TypeScript files and re-compiling when it sees them.


Here are the test related scripts:
* `npm test` - compiles, runs and watches the karma unit tests

